/*
	Project: decorating-lives
	Authors: Yemi Kehinde
*/

// --------------------------------------------- //
// DEFINE GLOBAL LIBS                            //
// --------------------------------------------- //
// Uncomment the line below to expose jQuery as a global object to the usual places
// window.jQuery = window.$ = require('./libs/jquery/jquery-1.10.2.js');

// force compilation of global libs that don't return a value.
require("./helpers/log");


//initialise KO object
var KO = {};

KO.Config = {
	variableX : '', // please don't keep me - only for example syntax!

	init : function () {
		console.debug('Kickoff is running');

		// Example module include
		KO.UI = require('./modules/UI');
		KO.UI.init();
	}
};
$(function() {
	KO.Config.init();



	var menuVisible = $('.isShown'),
		menuButton 	= $('.toggleButton'),
		menu 		= $('.navigation');

	$(menuButton).on('click', function() {
		menu.slideToggle(400);
		
	});

	// 	var	isOpen		= false;
	// 	var windowHeight = $(window).height();

	// 	$(menuButton).on('click', function(event) {
	// 		//prevent default action on the menu button
	// 		event.preventDefault();
	// 		menu.slideToggle(400);

	// 		if (isOpen) {
	// 			//closed
	// 				$(menu).css({'height': 'auto', 'display': 'none'});
	// 				isOpen = false;
					
	// 				return;

	// 			} else {
	// 				$(menu).css({"height": "100%", "position": "fixed", 'display': 'block'});
	// 				isOpen = true;
					
	// 				return;
	// 			};

	// 		//show dropdown

	// 		//hide the overflow
	// 		menu.toggleClass('show-mobile-menu');

	// 	});
});

