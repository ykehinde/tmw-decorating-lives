<?php
/* Template Name: events */
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * Please see /external/starkers-utilities.php for info on Starkers_Utilities::get_template_parts()
 *
 * @package 	WordPress
 * @subpackage 	Starkers
 * @since 		Starkers 4.0
 */
?>
<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/html-header' ) ); ?>
<?php if ( have_posts() ): ?>

<ul>
<?php query_posts('post_type=post'); query_posts('cat=2'); while ( have_posts() ) : the_post(); ?>
	<li>
		<article class="posts">
			<h1>
				<a href="<?php esc_url( the_permalink() ); ?>" title="<?php the_title(); ?>" rel="bookmark">
					<?php the_title(); ?>
				</a>
			</h1>

			<p class="meta" datetime="<?php the_time( 'Y-m-d' ); ?>" pubdate>
				<?php the_date(); ?> <?php the_time(); ?>
			</p> 
			
			<?php the_content(); ?>

		</article>
	</li>
<?php endwhile; ?>
</ul>
<?php else: ?>
<h2>No posts to display</h2>
<?php endif; ?>
 


<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/html-footer' ) ); ?>