<nav class="navigation right">
    <ul class="navigation--list clearfix">
        <li><a href="<?php echo esc_url( home_url( '/' ) ); ?>">Home</a></li>
        <li><a href="<?php echo esc_url( home_url( '/events' ) ); ?>">Events</a></li>
        <li><a href="<?php echo esc_url( home_url( '/news' ) ); ?>">News</a></li>
        <li><a href="<?php echo esc_url( home_url( '/fundraising-ideas' ) ); ?>">Fundraising Ideas</a></li>
        <li><a href="<?php echo esc_url( home_url( '/contact' ) ); ?>">Contact us</a></li>
    </ul>
</nav>
<header class="masthead clearfix" role="banner">
            <div class="toggle">
                <a href="#" class="toggleButton">
                    <img src="/images/burger.jpg" alt="" class="burger">
                </a>
            </div>

            <a href="http://www.tmwunlimited.com/" target="_blank">
                <img src="/images/tmw.png" class="main-logo tmw" alt="">
            </a>
            <a href="http://centrepoint.org.uk/" target="_blank">
                <img src="/images/centre-point-logo.png" class="main-logo" alt="">
            </a>