<footer class="footer" role="contentinfo">
			<div class="center">
				<ul class="footer-list">
					<li class="footer-link"><a href="<?php echo esc_url( home_url( '/contact' ) ); ?>">Contact us</a></li>
					<li class="footer-link"><a href="http://centrepoint.org.uk/" target='_blank'>Centre Point</a></li>
					<li class="footer-link"><a href="http://www.tmwunlimited.com/" target='_blank'>TMW Unlimited</a></li>
				</ul>
			</div>
		</footer>