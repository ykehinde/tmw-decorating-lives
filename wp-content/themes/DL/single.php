<?php
/**
 * The Template for displaying all single posts
 *
 * Please see /external/starkers-utilities.php for info on Starkers_Utilities::get_template_parts()
 *
 * @package 	WordPress
 * @subpackage 	Starkers
 * @since 		Starkers 4.0
 */
?>
<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/html-header' ) ); ?>

		<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

		<article>
			<div class="content">
                <div class="header">
                    <h1><?php the_title(); ?></h1>
                </div>

                <div class="meta">
                    <p>
                        <?php the_tags('<span>Tags: </span>', ', ', ''); ?></i>
                    </p>
                </div>

                <div class="post">
                    <?php the_content(); ?>
                </div>
            </div>
        </article>
		<?php endwhile; ?>

<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/html-footer' ) ); ?>
