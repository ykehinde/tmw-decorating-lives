<?php
/**
 * The template used to display Tag Archive pages
 *
 * Please see /external/starkers-utilities.php for info on Starkers_Utilities::get_template_parts()
 *
 * @package 	WordPress
 * @subpackage 	Starkers
 * @since 		Starkers 4.0
 */
?>
<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/html-header' ) ); ?>

		<?php if ( have_posts() ): ?>
		<h2 class="header">Tag Archive: <?php echo single_tag_title( '', false ); ?></h2>
		<ul class="archivePosts">
		<?php while ( have_posts() ) : the_post(); ?>
			<li>
				<article class="archive">
					<h2><a href="<?php esc_url( the_permalink() ); ?>" title="Permalink to <?php the_title(); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
					<time datetime="<?php the_time( 'Y-m-d' ); ?>" pubdate><?php the_date(); ?> <?php the_time(); ?></time> <?php comments_popup_link('Leave a Comment', '1 Comment', '% Comments'); ?>
					<p><?php the_content(); ?></p>
				</article>
			</li>
		<?php endwhile; ?>
		</ul>
		<?php else: ?>
		<h2>No posts to display in <?php echo single_tag_title( '', false ); ?></h2>
		<?php endif; ?>

<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/html-footer' ) ); ?>