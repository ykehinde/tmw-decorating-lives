### Overview ###
* Technology: HTML, CSS, PHP
* Database: 
* Architecture:

### Environments ###
Live: http://www.decoratinglives.co.uk

### LIVE Server: ###
* url: 
* web server: 
* db server: 
* db user: 
* db password: 

### Analytics information if in use ###


### Source Control ###
Bitbucket: [https://bitbucket.org/tmwagency/tmw-decorating-lives https://bitbucket.org/tmwagency/tmw-decorating-lives]

### Project Documentation ###
Nothing to report...

### The Team ###
* Technical Lead:
* Frontend Developer: Yemi Kehinde
* Backend Developer:
* QA:
* Content Loader:

### Notes ###
Nothing to report...